# java-hello-world
[![Generic badge](https://img.shields.io/badge/Fase-1-ff69b4.svg)](https://shields.io/)
[![Generic badge](https://img.shields.io/badge/Capitulo-2.%20Hello%20World-blue.svg)](https://shields.io/)
[![Generic badge](https://img.shields.io/badge/Made%20with-JAVA-orange.svg)](https://shields.io/)

Projeto com instruções básicas do Java

## :rocket:  Primeiros passos no Java:

* Olá Mundo!
* Tipos Primitivos
* Operadores de Atribuição
* Operadores de Incremento e Decremento
* Operadores de Igualdade e Relacionais
* Operadores Lógicos
* Fluxo de Controle (Estrutura de seleção (if))
* Entrada de Dados (Scanner)
