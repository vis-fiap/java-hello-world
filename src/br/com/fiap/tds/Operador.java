package br.com.fiap.tds;

public class Operador {

	public static void main(String[] args) {
		
		int x = 5;
		int y = 10;
		int z = 15;
		
		System.out.println("-------- OPERADORES ARITMÉTICOS --------");
		System.out.println(x+y);
		System.out.println(x-y);
		System.out.println(x*y);
		System.out.println(x/y);
		System.out.println(x%y);
		System.out.println(z - x + y * (x / y));

		System.out.println("-------- OPERADORES ATRIBUIÇÃO --------");
		int w = 10;
		w += 15; // w = w + 15 - atribuição aditiva
		System.out.println(w);
		w -= 15; // w = w - 15 - atribuição subtrativa
		System.out.println(w);
		w *= 15; // w = w * 15
		System.out.println(w);
		w /= 15; // w = w / 15
		System.out.println(w);
		
		System.out.println("-------- OPERADORES INCREMENTO E DECREMENTO --------");
		System.out.println(w);
		w++; // w = w + 1; resultado de incremento de w
		System.out.println(w);
		w--; // w = w - 1; resultado de decremento de w
		System.out.println(w);
		
		++w; // igual a w++
		System.out.println(w);
		--w; // igual a w--
		System.out.println(w);
		
		int a = 3;
		w = a++;
		System.out.println("w = " + w);
		System.out.println("a = " + a);
		
		a = 3;
		w = --a;
		System.out.println("w = " + w);
		System.out.println("a = " + a);
		
		System.out.println("---------- OPERADORES LÓGICOS ----------");
		int idade = 18;
		boolean precisaVotar = idade >= 18 && idade <= 70;
		System.out.println(precisaVotar);
		
		int nrAmarelo = 1, nrVermelho = 0;
		boolean suspenso = nrAmarelo == 2 || nrVermelho == 1;
		System.out.println(suspenso);
		
		x = 9;
		y = 11;
		boolean teste = x > 10 ^ y > 10; // ou exclusivo, APENAS uma das condições deve ser verdadeira, ou false, se as duas foram iguais o retorno vai ser false
		System.out.println(teste);
		
		idade = 20;
		boolean maiorIdade = !(idade >= 18);
		System.out.println(maiorIdade);
		
	}
	
}
