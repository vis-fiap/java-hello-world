package br.com.fiap.tds;

public class Variaveis {

	public static void main(String[] args) {
		
		// Tipos primitivos inteiros
		int idade = 10;
		double preco = 9.99;
		char genero = 'M';
		boolean temFilhos = true;
		
		System.out.println("---------------------\n"
				+ "TIPOS PRIMITVOS\n"
				+ "---------------------");
		System.out.println(idade);
		System.out.println(preco);
		System.out.println(genero);
		System.out.println(temFilhos);
		
		System.out.println("---------------------\n"
				+ "CONVERSÃO E CAST\n"
				+ "---------------------");
		float floatRecebeInt = idade; // conversao
		System.out.println(floatRecebeInt);
		int intRecebeDouble = (int) preco; // cast
		System.out.println(intRecebeDouble);
	}
	
}
