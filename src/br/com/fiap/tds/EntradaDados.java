package br.com.fiap.tds;

import java.util.Scanner;

public class EntradaDados {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("***** MENU *****");
		System.out.println("1 - Verificar Idade");
		System.out.println("2 - Verificar IMC");
		System.out.println("Digite a opção desejada: ");
		byte opcao = sc.nextByte();

		switch (opcao) {
		case 1:
			verificarIdade(sc);
			break;
		case 2:
			verificarImc(sc);
			break;
		default:
			System.out.println("Opção inválida!");
			break;
		}
		
		sc.close();
		
	}

	private static void verificarImc(Scanner sc) {

		System.out.println("******** IMC ********\n");

		System.out.println("Digite o peso: ");
		float peso = sc.nextFloat();

		System.out.println("Digite a altura: ");
		float altura = sc.nextFloat();

		float imc = peso / (altura * altura);

		if (imc < 18.5) {
			System.out.println("Está abaixo do peso ideal");
		}
		if (imc >= 18.5 && imc <= 25) {
			System.out.println("Peso é ideal");
		} else {
			System.out.println("Está fora do peso ideal. O seu IMC é: " + imc);
		}
	}

	private static void verificarIdade(Scanner sc) {

		System.out.println("Digite a idade: ");
		short idade = sc.nextShort();

		System.out.println("Digite o nome: ");
		String nome = sc.nextLine();
		
		System.out.println("Nome: ".concat(nome));
		if (idade < 18) {
			System.out.println("Menor de idade");
		} else if (idade >= 18 && idade <= 50) {
			System.out.println("Adulto");
		} else {
			System.out.println("Idade da sabedoria");
		}
	}

}
